/**
 * Created by ������� on 11.10.2015.
 */
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int n = 9;
        String value;

        Scanner input = new Scanner( System.in );

        System.out.println( "Enter any numeric value from 0 to 9: " );

        n = input.nextInt();

        switch(n) {
            case 0: value = "Zero";
                    break;
            case 1: value = "One";
                    break;
            case 2: value = "Two";
                    break;
            case 3: value = "Three";
                    break;
            case 4: value = "Four";
                    break;
            case 5: value = "Five";
                    break;
            case 6: value = "Six";
                    break;
            case 7: value = "Seven";
                    break;
            case 8: value = "Eight";
                    break;
            case 9: value = "Nine";
                    break;
            default: value = "Invalid value";
                     break;
        }

        System.out.println( value );
    }
}
