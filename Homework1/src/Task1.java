/**
 * Created by ������� on 11.10.2015.
 */
import java.util.*;
public class Task1 {
    public static void main(String[] args) {

        int n;

        Scanner input = new Scanner( System.in );

        System.out.println( "Enter positive integer value: " );

        n = input.nextInt();

        Task1 obj = new Task1();

        System.out.println( "The factorial of " + n + " equals " + obj.factorial(n));
    }

    public int factorial(int num) {
        int fact = 1;
        for(int i = 0; i <= num; i++)
            if (i == 0)
                fact = 1;
            else
                fact *= i;
        return fact;
    }
}
