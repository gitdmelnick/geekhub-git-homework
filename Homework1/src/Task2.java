import java.util.Scanner;

/**
 * Created by ������� on 11.10.2015.
 */
public class Task2 {
    public static void main(String[] args) {
        int n;

        Scanner input = new Scanner( System.in );

        System.out.println( "Enter positive integer value: " );

        n = input.nextInt();

        Task2 obj = new Task2();

        System.out.println( "The value of " + n + "th Fibonacci number equals " + obj.fibonacci(n));
    }

    public int fibonacci(int num) {
        int f0 = 0, f1 = 1, current;
        for (int i = 2; i < num; i++) {
            current = f1 + f0;
            f0 = f1;
            f1 = current;
        }
        if (num == 0)
            f1 = 0;
        return f1;
    }
}
